# Assignment #
```
Please use JAVA for the assignment, other tools you can choose yourself.

Looking forward to your answer.

Tasks:
1. Correct all of the deficiencies in index.html

2. "Sectors" selectbox:
2.1. Add all the entries from the "Sectors" selectbox to database
2.2. Compose the "Sectors" selectbox using data from database

3. Perform the following activities after the "Save" button has been pressed:
3.1. Validate all input data (all fields are mandatory)
3.2. Store all input data to database (Name, Sectors, Agree to terms)
3.3. Refill the form using stored data
3.4. Allow the user to edit his/her own data during the session

Write us Your best code!

After completing the tasks, please provide us with:
1. Full database dump (structure and data)
2. Source code
```

Original assignment with ***index.html*** can be found in the project directory `/assignment`.

# Solution

### For starting application do next:
1. checkout project: `git clone https://_george@bitbucket.org/_george/helmes-test.git`
1. `mvnw package`
1. `cd target`
1. `java -jar assignment.jar`
1. open in the browser: http://localhost:8080

Time: 10-12h

Difficult parts:

* displaying nested list in the select input. Thymeleaf doesn't support value auto-setting in the fragments.

*Test coverage is not full. Has been done just few tests for showing skills.*
package ee.helmes.service;

import ee.helmes.domain.Sector;
import ee.helmes.repository.SectorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SectorService {

    @Autowired
    SectorRepository sectorRepository;

    // TODO: add caching
    public List<Sector> getAllHierarchical() {
        List<Sector> result = new ArrayList<>();
        sectorRepository.findByParent(null).forEach(s -> addToResult(s, result));
        return result;
    }

    private void addToResult(final Sector sector, final List<Sector> result) {
        result.add(sector);
        if (!sector.getChildren().isEmpty()) {
            sector.getChildren().forEach(c -> addToResult(c, result));
        }
    }
}

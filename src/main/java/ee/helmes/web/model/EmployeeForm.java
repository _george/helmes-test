package ee.helmes.web.model;

import ee.helmes.domain.Sector;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

public class EmployeeForm {

    private Long id;

    @NotNull(message = "Name should not be empty")
    @Size(min = 3, message = "Name should be at least 3 characters")
    private String name;

    @NotNull
    @Size(min = 1, message = "At least one sector should be selected")
    private List<Sector> sectors;

    @AssertTrue(message = "You should agree with terms")
    private boolean termsAgree;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Sector> getSectors() {
        return sectors;
    }

    public void setSectors(List<Sector> sectors) {
        this.sectors = sectors;
    }

    public boolean isTermsAgree() {
        return termsAgree;
    }

    public void setTermsAgree(boolean termsAgree) {
        this.termsAgree = termsAgree;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

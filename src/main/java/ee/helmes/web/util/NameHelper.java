package ee.helmes.web.util;

import ee.helmes.domain.Sector;
import org.springframework.stereotype.Component;

import java.util.Collections;

@Component
public class NameHelper {

    public String getSectorOptionName(Sector sector) {
        String name = String.join("", Collections.nCopies(sector.getLevel(), "&nbsp;&nbsp;&nbsp;&nbsp;"));
        return name.concat(" ").concat(sector.getName());
    }
}

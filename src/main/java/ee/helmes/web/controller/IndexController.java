package ee.helmes.web.controller;

import ee.helmes.domain.Employee;
import ee.helmes.domain.Sector;
import ee.helmes.service.EmployeeService;
import ee.helmes.service.SectorService;
import ee.helmes.web.model.EmployeeForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/")
public class IndexController {

    public static final String SUCCESS_TEMPLATE = "index";

    @Autowired
    private SectorService sectorService;

    @Autowired
    private EmployeeService employeeService;

    @ModelAttribute("allSectors")
    public List<Sector> addAllSectors() {
        return sectorService.getAllHierarchical();
    }

    @RequestMapping(method = RequestMethod.GET)
    public String index(Model model) {
        model.addAttribute("form", new EmployeeForm());
        return SUCCESS_TEMPLATE;
    }

    @RequestMapping(method = RequestMethod.POST)
    public String save(Model model, @ModelAttribute("form") @Valid EmployeeForm form, Errors errors) {
        if (!errors.hasErrors()) {
            Employee employee = employeeService.save(convert(form));
            form.setId(employee.getId());
            model.addAttribute("message", "Data has been successfully saved!");
        }

        model.addAttribute("form", form);

        return SUCCESS_TEMPLATE;
    }

    // might be used Dozer instead
    private Employee convert(EmployeeForm form) {
        Employee employee = new Employee();
        employee.setId(form.getId());
        employee.setName(form.getName());
        employee.setSectors(form.getSectors());
        return employee;
    }

}

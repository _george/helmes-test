CREATE TABLE employee (
  id   INT AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(255) NOT NULL
);

CREATE TABLE employee_sectors (
  employee_id INT NOT NULL,
  sectors_id  INT NOT NULL
);

CREATE TABLE sector (
  id     INT AUTO_INCREMENT PRIMARY KEY,
  name   VARCHAR(150) NOT NULL,
  parent INT,
  level  SMALLINT
);

ALTER TABLE sector ADD CONSTRAINT sector_parent_fk FOREIGN KEY (parent) REFERENCES sector (id);

ALTER TABLE employee_sectors ADD CONSTRAINT employee_sectors_employee_fk FOREIGN KEY (employee_id) REFERENCES employee (id);
ALTER TABLE employee_sectors ADD CONSTRAINT employee_sectors_sectors_fk FOREIGN KEY (sectors_id) REFERENCES sector (id);
package ee.helmes.repository;

import ee.helmes.domain.Sector;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import javax.validation.ConstraintViolationException;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@DataJpaTest
public class SectorRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private SectorRepository repository;


    @Test(expected = ConstraintViolationException.class)
    public void insertEmptyLevel() throws Exception {
        Sector sector = new Sector();
        sector.setName("Sector name");
        this.entityManager.persist(sector);
    }

    @Test
    public void findAllTest() throws Exception {
        List<Sector> list = this.repository.findAll();
        assertEquals(79, list.size());
    }
}
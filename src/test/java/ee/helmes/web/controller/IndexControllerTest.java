package ee.helmes.web.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest()
public class IndexControllerTest {

    @Autowired
    private WebApplicationContext context;

    private MockMvc mvc;

    @Before
    public void setup() {
        mvc = MockMvcBuilders.webAppContextSetup(context).build();
    }

    @Test
    public void indexTest() throws Exception {
        mvc.perform(get("/"))
                .andExpect(model().attribute("allSectors", hasSize(79)))
                .andExpect(view().name("index"));
    }

    @Test
    public void saveTest() throws Exception {
        mvc.perform(post("/")
                .param("name", "Georgii")
                .param("sectors", "1")
                .param("sectors", "2"))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("form"));
    }
}
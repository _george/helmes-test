package ee.helmes.web.util;

import ee.helmes.domain.Sector;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class NameHelperTest {
    @Test
    public void getSectorOptionName() throws Exception {
        Sector sector = new Sector();
        sector.setName("Name");
        sector.setLevel(2);

        String actual =  new NameHelper().getSectorOptionName(sector);
        assertEquals("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Name", actual);
    }

}